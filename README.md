# robot1

Project name:	Voice Controlled Robot on GoPiGo2 frame (Häälkäsklusega juhitav GoPiGo2 robot)

Team members:	Magnus Kaldjärv, Risto Künnapas

***

## Overview

As project name says, our mission is to set roaming a 2-wheeled voice controlled robot. To simplify it for us and to bring out our robots main functionality, only sensors we are going to use would be microphone sound sensor and GoPiGo2 built-in wheel movement tracking encoder sensors. Our final objective is to get this robot to complete various tasks, such as turning for certain degrees, ride forward or backward for specified distance and change its moving speed on flat smooth floor as vocally commanded. With this kind of control we should be able to navigate it between obstacles and around in room as needed. 

After considering the task in our hand we decided to divide project into phases:

1. Deciding what kind of voice recognision software we should use, 
keeping in mind aspect such as:
	+ in what language voice orders are given
	+ can this program be handled by RaspberryPi or should we run it on networked computer
	+ what is our previous programming experience
	+ what we can get our hands on(probably only open-source software)
2. Getting to know voice recognision software's documentation
3. Deciding, what would be those commands we are going to give to robot over voice
4. Writing baseprogram what should at least try to perform our task - look for commands and gives commands for platform to move accordingly
5. Preset RaspberryPi, download and install needed software
6. Build our robotplatform
7. Getting our robot to recognize its orders and fulfill them
8. Trying to improve robots performance and picking out and smoothing buggy parts 

***

## Schedule

Dates | What should be done
--- | --
09.04-15.04 | picking software, learning its documentation 
16.04-22.04 | deciding vocal commands, writing baseprogram 
23.04-29.04 | writing baseprogram 
30.04-06.05 | setting up raspberryPi, building robot
03.05 | Proof of consept
07.05-13.05 | getting robot running, debugging
14.05-20.05 | improvement and smoothing
18.05 | Project poster
21.05 | Project presentation with poster

***

## Component list

Component | Amount
--- | ---
RaspberryPi 2* or 3 | 1
GoPiGo2 platform | 1
The microphone sound sensor module | 1
connection wires for microphone | needs specifying
Battery for RaspberryPi | 1 
connection wires for battery | 1

*for RaspberryPi 2 might be needed an USB Wifi stick
*enlisted parts will be expected to be recived by instructors

***

## Final solution

For voice recognition software we finaly decided to use Google Assistant API(https://developers.google.com/assistant/sdk/).
To make it easier for us, we used shivasiddharth's "GassistPi -- Google Assistant for all Raspberry Pi Boards"(https://github.com/shivasiddharth/GassistPi/blob/master/README.md) as a base.
Our code should be imported into directory "/home/GassistPi/src/" and file "main.py" should be overwritten.
Additionaly to voice commands we made script for robot to randomly roam around and when encountering an obstacle in front, it turns right. Dispite our best effort we didn't manage to integrate it into voice command script.
