#!/usr/bin/env python

import time
import gopigo as go
from actions import say

whindex=5

def getCommand(usrcmd):
    pieces = str(usrcmd).split("'")[3].split()
    return pieces

def checkEncoder():
    while go.read_enc_status():
        time.sleep(.1)


def forward(usrcmd):
    pieces = getCommand(usrcmd)
    print(pieces[1])
    try:
        number = int(pieces[1])
        print(number)

        go.enable_encoders()

        if pieces[2]=="centimeters" or pieces[2]=="cm" or pieces[2]=="centimeter":
            go.enc_tgt(1,1,int(number*whindex*18/100))
            print(int(number*whindex*18/100))
            go.fwd()
        if pieces[2]=="meters" or pieces[2]=="m" or pieces[2]=="meter":
            go.enc_tgt(1,1,number*whindex*18)
            go.fwd()
            
        checkEncoder()

    except:
        print(pieces[2])

        if pieces[1]=="continuously":
            go.fwd()

    print("I like it")
    #say("Straight into abyss")

def backward(usrcmd):
    pieces = getCommand(usrcmd)
    try:
        number = int(pieces[1])
        print(number)

        go.enable_encoders()

        if pieces[2]=="centimeters" or pieces[2]=="cm" or pieces[2]=="centimeter":
            go.enc_tgt(1,1,number*whindex*18/100)
            go.bwd()
        if pieces[2]=="meters" or pieces[2]=="m" or pieces[2]=="meter":
            go.enc_tgt(1,1,number*whindex*18)
            go.bwd()
        #time.sleep(number)
        #go.stop()
        checkEncoder()

    except:
        print(pieces[2])

        if pieces[1]=="continuously":
            go.bwd()

    print("I like it")
   # say("Adios, amigos!")

def setspeed(usrcmd):
    pieces = getCommand(usrcmd)
    if pieces[1]=="speed":
        try:
            go.set_speed(int(pieces[2]))
            say("new speed is "+str(pieces[2]))
        except:
            print()
            say("something went wrong")
    if pieces[1]=="right":
        try:
            go.set_right_speed(int(pieces[3]))
            say("new right speed is "+str(pieces[3]))
        except:
            print() 
            say("something went wrong")
    if pieces[1]=="left":
        try:
            go.set_left_speed(int(pieces[3]))
            say("new left speed is "+str(pieces[3]))
        except:
            print()
            say("something went wrong")

def turn(usrcmd):
    pieces = getCommand(usrcmd)
    go.enable_encoders()
    try:
        if pieces[1]=="right":
            try:
                number = int(pieces[2])
                print(number)

                go.enc_tgt(1,1,int(number*18*0.4/90))
                go.right_rot()

                checkEncoder()
               # say("turning right")

            except:
                print(pieces[2])
                number=0.36

                go.enc_tgt(1,1,int(number*18))
                go.right_rot()

                checkEncoder()
            
        if pieces[1]=="left":
            try:
                number = int(pieces[2])
                print(number)

                go.enc_tgt(1,1,int(number*18*0.4/90))
                go.left_rot()
            
                checkEncoder()

            except:
                print(pieces[2])
                number=0.36

                go.enc_tgt(1,1,int(number*18))
                go.left_rot()

                checkEncoder()
    except:
        say("I won't")


def square(usrcmd):
    pieces = getCommand(usrcmd)
    go.enable_encoders()
    try:
        i=1
        while i<5:
            go.enc_tgt(1,1,30*whindex*18/100)
            go.fwd()
            checkEncoder()
            go.enc_tgt(1,1,int(90*18*0.4/90))
            go.right_rot()
            checkEncoder()
            i=i+1


        say("Uuuu ihhhh, you touch my trallallaaa...")
            
            
    except:
        print()
        say("something went wrong")
    


