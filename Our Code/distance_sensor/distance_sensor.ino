// Eesmise sensori pinnid
#define TRIG1 5
#define ECHO1 4

//Korval sensori pinnid
#define TRIG2 3
#define ECHO2 2



#define MAX_DIST_TIMEOUT 50000
#define MIN_PULSE_DURATION 200


// Eesmise sensori
long duration1;
long distance_in_mm1;


//Korval sensori
long duration2;
long distance_in_mm2;

void setup()
{
  pinMode(TRIG1, OUTPUT);
  pinMode(ECHO1, INPUT);
  digitalWrite(TRIG1, LOW);

  pinMode(TRIG2, OUTPUT);
  pinMode(ECHO2, INPUT);
  digitalWrite(TRIG2, LOW);


  Serial.begin(9600);
}

void loop()
{
  digitalWrite(TRIG1, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIG1, LOW);

  duration1 = pulseIn(ECHO1, HIGH);

  delayMicroseconds(100);

  digitalWrite(TRIG2, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIG2, LOW);

  duration2 = pulseIn(ECHO2, HIGH);

  //Eesmise sensori
  //if (duration1 > MIN_PULSE_DURATION)
  //{
    // calculate the distance in millimetres (mm)
    distance_in_mm1 = duration1 / 2 * 340 / 1000;
    distance_in_mm2 = duration2 / 2 * 340 / 1000;
    // write the distance in mm to serial using JSON format
    Serial.print("{\"distance_eesmise1\":");
    Serial.print(distance_in_mm1);
   // Serial.println("}");
  //}

  //Korval sensori

 //if (duration2 > MIN_PULSE_DURATION)
  //{
    // calculate the distance in millimetres (mm)

    // write the distance in mm to serial using JSON format
    Serial.print(",\"distance_korval2\":");
    Serial.print(distance_in_mm2);
    Serial.println("}");
  //}
  delay(200);
}






