
import gopigo as go
import serial
import time
import json
import timeit
from robot import checkEncoder
from random import randint
import socket

s = socket.socket()        
host = socket.gethostname()
port = 15555 

timer=0
counting=1
start_time=0
turning=2
turn_time=timeit.default_timer()
go.set_left_speed(120)
go.set_right_speed(100)
life = 1


try:
    # Open a serial connection with the Arduino
    ser=serial.Serial('/dev/ttyUSB0', 9600, timeout=1)

    while True:
        #try:
        #    s.connect((host, port))
        #try:
        #    life = life + int(s.recv(1024))
        
        #print(life)
        if life==counting and timer==0:
            go.fwd()
            
            # Read a single line from serial
            serial_line = ser.readline()

            # Try decoding the received JSON data
            try:
                data = json.loads(serial_line)
                # Extract the distance value
                distance_eesmise1 = data['distance_eesmise1']
                print(distance_eesmise1)
                distance_korval2 = data['distance_korval2']
                # Print it to the console
                print "Distance in front is ", distance_eesmise1, "mm"
                print "Distance in sideways is ", distance_korval2, "mm"

                if distance_eesmise1 < 500:
                    go.enable_encoders()

                    go.stop()
                    number=0.36

                    go.enc_tgt(1,1,int(number*18))
                    go.right_rot()

                    checkEncoder()


                if distance_korval2 < 500:
                    go.enable_encoders()

                    go.stop()
                    number=0.1

                    go.enc_tgt(1,1,int(number*18))
                    go.right_rot()

                    checkEncoder()

            # If there is any any problem parsing the JSON, notify user
            except ValueError, e:
                print "JSON ValueError caught. Proceeding without changes."

            #currently program needs to be killed and shot down from the terminal window (python - import gopigo as go- go.stop())
                

            if (timeit.default_timer()-turn_time)>=turning:
                go.enable_encoders()
                number=float(randint(-9,9))*0.1
                print number
                if number>=0:
                    go.enc_tgt(1,1,int(number*18))
                    go.right_rot()

                    checkEncoder()
                else:
                    go.enc_tgt(1,1,int((-number)*18))
                    go.left_rot()

                    checkEncoder()
                turn_time=timeit.default_timer()
                turning=randint(1,6)




                
    if life!=counting:
        counting=counting+1
        start_time=timeit.default_timer()
        timer=1
        go.stop()

    

    if (timeit.default_timer()-start_time)>=30:
        timer=0
        
            
                            

except Exception as e:
    print e
    print "Exiting program"
    print "Serial closed"
    stop()
    s.close()
